import Vue from 'vue'
import App from './App.vue'

import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'//element-ui的css
import axios from 'axios';
import './assets/icon/iconfont.css'
import './assets/element/index.css'
import store from './components/VueX/store'
// require('../mock');
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)
Vue.use(ElementUI)

Vue.config.productionTip = false

const i18n = new VueI18n({
  locale: 'zh', // 定义默认语言为中文 
  messages: {
    'zh': require('@/assets/i18n/zh.json'),
    'en': require('@/assets/i18n/en.json')
  }
});

new Vue({
  render: h => h(App),
}).$mount('#app')
